<?php

declare(strict_types = 1);

use Gupo\DBQuery\Contracts\ConnectionManagerContract;
use Gupo\DBQuery\Contracts\QueriesContract;
use Gupo\DBQuery\Foundation\ConnectionManager;
use Gupo\DBQuery\Foundation\Queries;

return [
    /**
     * DBQuery cache
     */
    'cache' => [
        'enabled' => env('YDQ_CACHE_ENABLED', false),

        'driver' => env('YDQ_CACHE_DRIVER', env('CACHE_DRIVER')),
    ],

    'dependencies' => [
        ConnectionManagerContract::class => ConnectionManager::class,
        QueriesContract::class           => Queries::class,
    ],
];
