<?php

declare(strict_types = 1);

namespace Gupo\DBQuery\Exceptions;

final class DBClientConnectionException extends \Exception implements \Throwable
{
}
